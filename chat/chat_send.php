<?php
include '../includes/config.php';
session_start();

$msg = $_GET['msg'];
$nick = $_GET['nick'];
$gameid = $_SESSION['gameid'];

$stmt = $conn->prepare("INSERT INTO chat (ts, nickname, message, ref_game) SELECT NOW(), ?, ?, ?");
$stmt->bind_param("ssi", $nick, $msg, $gameid);
$stmt->execute();
?>