-- phpMyAdmin SQL Dump
-- version 4.6.6
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Apr 25, 2017 at 10:22 PM
-- Server version: 10.1.20-MariaDB
-- PHP Version: 7.0.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `id1217091_planningpoker`
--

-- --------------------------------------------------------

--
-- Table structure for table `activeplayers`
--

DROP TABLE IF EXISTS `activeplayers`;
CREATE TABLE `activeplayers` (
  `active_id` bigint(20) NOT NULL,
  `ref_player` bigint(20) NOT NULL,
  `ref_game` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `card`
--

DROP TABLE IF EXISTS `card`;
CREATE TABLE `card` (
  `card_id` bigint(20) NOT NULL,
  `card_value` int(11) DEFAULT NULL,
  `ref_player` bigint(20) DEFAULT NULL,
  `ref_game` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `chat`
--

DROP TABLE IF EXISTS `chat`;
CREATE TABLE `chat` (
  `msg_id` bigint(20) NOT NULL,
  `nickname` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ts` datetime DEFAULT NULL,
  `message` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ref_game` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `game`
--

DROP TABLE IF EXISTS `game`;
CREATE TABLE `game` (
  `game_id` bigint(20) NOT NULL,
  `game_name` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ref_moderator` bigint(20) DEFAULT NULL,
  `started` datetime DEFAULT NULL,
  `gamecode` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `user_story` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gamestate` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `player`
--

DROP TABLE IF EXISTS `player`;
CREATE TABLE `player` (
  `player_id` bigint(20) NOT NULL,
  `player_name` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `createdAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `activeplayers`
--
ALTER TABLE `activeplayers`
  ADD PRIMARY KEY (`active_id`),
  ADD UNIQUE KEY `UniqueActive` (`ref_player`,`ref_game`),
  ADD KEY `activeplayers_ibfk_2` (`ref_game`);

--
-- Indexes for table `card`
--
ALTER TABLE `card`
  ADD PRIMARY KEY (`card_id`),
  ADD UNIQUE KEY `card_id` (`card_id`),
  ADD UNIQUE KEY `UniqueCard` (`ref_player`,`ref_game`),
  ADD KEY `FK_gamecard` (`ref_game`),
  ADD KEY `ref_player` (`ref_player`);

--
-- Indexes for table `chat`
--
ALTER TABLE `chat`
  ADD PRIMARY KEY (`msg_id`),
  ADD KEY `chat_ibfk_1` (`ref_game`);

--
-- Indexes for table `game`
--
ALTER TABLE `game`
  ADD PRIMARY KEY (`game_id`),
  ADD UNIQUE KEY `gamecode` (`gamecode`),
  ADD KEY `ref_moderator` (`ref_moderator`);

--
-- Indexes for table `player`
--
ALTER TABLE `player`
  ADD PRIMARY KEY (`player_id`),
  ADD UNIQUE KEY `player_name` (`player_name`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `activeplayers`
--
ALTER TABLE `activeplayers`
  MODIFY `active_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=879;
--
-- AUTO_INCREMENT for table `card`
--
ALTER TABLE `card`
  MODIFY `card_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=206;
--
-- AUTO_INCREMENT for table `chat`
--
ALTER TABLE `chat`
  MODIFY `msg_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=85;
--
-- AUTO_INCREMENT for table `game`
--
ALTER TABLE `game`
  MODIFY `game_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=76;
--
-- AUTO_INCREMENT for table `player`
--
ALTER TABLE `player`
  MODIFY `player_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=205;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `activeplayers`
--
ALTER TABLE `activeplayers`
  ADD CONSTRAINT `activeplayers_ibfk_1` FOREIGN KEY (`ref_player`) REFERENCES `player` (`player_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `activeplayers_ibfk_2` FOREIGN KEY (`ref_game`) REFERENCES `game` (`game_id`) ON DELETE CASCADE;

--
-- Constraints for table `card`
--
ALTER TABLE `card`
  ADD CONSTRAINT `FK_gamecard` FOREIGN KEY (`ref_game`) REFERENCES `game` (`game_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_playercard` FOREIGN KEY (`ref_player`) REFERENCES `player` (`player_id`) ON DELETE CASCADE;

--
-- Constraints for table `chat`
--
ALTER TABLE `chat`
  ADD CONSTRAINT `chat_ibfk_1` FOREIGN KEY (`ref_game`) REFERENCES `game` (`game_id`) ON DELETE CASCADE;

--
-- Constraints for table `game`
--
ALTER TABLE `game`
  ADD CONSTRAINT `game_ibfk_1` FOREIGN KEY (`ref_moderator`) REFERENCES `player` (`player_id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
