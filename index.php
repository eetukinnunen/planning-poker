<?php
include ('includes/config.php');
session_start();

$query = "DELETE FROM player WHERE createdAt < DATE_SUB(now(), INTERVAL 5 DAY)";
$result = mysqli_query($conn, $query);
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Planning Poker</title>
	<link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="css/new.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
</head>
<body>
<section class="container">
    <div class="login">
        <h1>Planning Poker Game</h1>
        <?php
            if (!isset($_SESSION['user']))
            {
        ?>
            <br/><h2>Create username</h2>
            <!--<img src="images/friends.gif" />-->
            <form method="post" action="valikko.php">
                <p><input type="text" name="login" placeholder="Username" required></p>
                <p class="submit"><input type="submit" name="commit" value="Log in"></p>
            </form>
        <?php
            }
            else if (isset($_SESSION['user']))
            {
                if (isset($_SESSION['gameid']))
                {
        ?>
                    <a href="/peli.php" class="button">Return to Game</a>
                    <p> </p>
            <?php
                }
            ?>
                <a href="/valikko.php" class="button">Go to Menu</a>
                <p> </p>
                <a href="logout.php" class="button">Log out</a>
        <?php
            }
        ?>
        <br /><br />
        <b>
            <p>Made by:</p>
            <p>Eetu Kinnunen, Julius Backman</p>
        </b>
    </div>

  </section>

  

</body>
</html>