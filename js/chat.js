setInterval(loadChat, 1000);
function loadChat()
{
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function()
    {
        if(this.readyState==4 && this.status==200)
        {
            var temp = this.responseText;
            temp = temp.replace(/\|/g, "</b>: ");
            temp = temp.replace(/\n/g, "<br><b>");
            document.getElementById('chatbox').innerHTML = "<b>"+temp;
        }
    }
    xhttp.open("GET", "chat/chatlog.php?ts="+(new Date()).getTime(), true);
    xhttp.send();
}
function sendMessage()
{
    var xhttp = new XMLHttpRequest();
    var req = "chat/chat_send.php?nick="+document.getElementById('nick').value;
    req = req + "&msg="+document.getElementById('msg').value;
    req = req + "&gameid="+document.getElementById('gameid').value;
    xhttp.open("GET", req, true);
    xhttp.send();
    document.getElementById('msg').value = '';
}
function checkKey(e)
{
    if(e.keyCode == 13)
    {
        sendMessage();
    }
}
