var gamestate = 0;
var cardcheck = 0;

// Tsekkaa, missa vaiheessa peli on
var storyInterval = setInterval(stateCheck, 1000);
function stateCheck()
{
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function()
    {
        if(this.readyState==4 && this.status==200)
        {
            gamestate = this.responseText;
        }
    }
    xhttp.open("GET", "queries/pelintila.php?ts="+(new Date()).getTime(), true);
    xhttp.send();

    displayStory();
}

// User storyn printtaus kaikille ja storyn olemassa olon tsekkaus
function displayStory()
{
    /*var xhttp5 = new XMLHttpRequest();
    xhttp5.onreadystatechange = function()
    {
        if(this.readyState==4 && this.status==200)
        {
            tarina = this.responseText;
            document.getElementById('tarina').innerHTML = tarina;
        }
    }
    xhttp5.open("GET", "queries/pelintila.php?ts="+(new Date()).getTime(), true);
    xhttp5.send();*/
    $.ajax
    ({
        type: "GET",
        url: "queries/lueuserstory.php",
        dataType: 'json',
        cache: false,
        success: function (data) {
            var tarina = data[0]; // Tarinan teksti
            storystate = parseInt(data[1]); // "boolean" siita onko tarina olemassa
            document.getElementById('tarina').innerHTML = tarina;
            if (storystate == 1) // User story loytyy
            {
                if (gamestate == 2)
                {
                    // $("#consensus").remove();
                }
                else
                {
                    cardExistence();
                    gameOn();
                }

            }
            else if (storystate == 0) // User storya ei loydy
            {
                clearCards(); // playercards.js
                gamestate = 0;

                $(function(){

                    $('.kortti').draggable( "disable" )
                });
            }
        }
    });
}

// Tarkistaa, onko user x asettanut pelissä y jo kortin
function cardExistence() // "cardcheck is not defined"
{
    var xhttp4 = new XMLHttpRequest();
    xhttp4.onreadystatechange = function()
    {
        if(this.readyState==4 && this.status==200)
        {
            // cardcheck = pelattukortti.php:sta saatu tulos
            cardcheck = parseInt(this.responseText);
            // alert(cardcheck);
        }
    }
    xhttp4.open("GET", "queries/pelattukortti.php?ts="+(new Date()).getTime(), true);
    xhttp4.send();
}

// Aloittaa saman user storyn alusta
function roundRestart()
{
    // Poista kortit
    clearGamecards(); // playercards.js
    // Tyhjenna pelaajakortit-alue
    clearCards(); // playercards.js
    // $("#consensus").remove();
    stateSet(1);
}

// Peli kayntiin
function gameOn()
{
        cardCount(); // playercards.js

        // If no card set with gameid & userid, let drag

        $(function(){
 
        $( ".kortti" ).draggable({

        revert : function(event, ui) {
           
                $(this).data("uiDraggable").originalPosition = {
                    top : 0,
                    left : 0
                };
                return !event;
            }
        });

        $( ".pelikentta" ).droppable({
        drop: function( event, ui ) {
         $(this).droppable('option', 'accept', '.kortti');
            $(".card").draggable('disable');
            var korttiarvo = parseInt(ui.draggable.text());
            // alert(korttiarvo);
            aktiiviKortti(korttiarvo); // cardplayed.js

    },
  out: function(event, ui){
        $(this).droppable('option', 'accept', '.kortti');      
        }       
        });
        });


            $(function(){
                if (cardcheck > 0) // Kortti on jo valittu, joten dragging pois
                {
                    $('.kortti').draggable( "disable" )
                }
                else // Korttia ei ole valittu joten dragging paalle
                {
                    $('.kortti').draggable( "enable" )
                }
            });
            if (gamestate == 2 || gamestate == 3) // Kaikki ovat valinneet kortin
            {
                $(function(){

                    $('.kortti').draggable( "disable" )
                });

                // Printaa nakyville kaikkien pelaajien valitsemat arvot

                // Jos arvot tasmaavat, peli on ohi ja user storyksi tulee null. Gamestateksi muu kuin 3

                // Jos arvot eivat tasmaa, laita esille "start new round"-nappi, jossa kaikki muu nollataan paitsi user story. Gamestateksi muun kuin 3
            }

}