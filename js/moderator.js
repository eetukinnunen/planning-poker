//// MODERAATTORIN JUTUT

// Asettaa game staten
function stateSet(newstate)
{
    var xhttp = new XMLHttpRequest();
    xhttp.open("GET", "queries/pelintila.php?newstate="+newstate, true);
    xhttp.send();
}

// User storyn luonti ja siihen liittyvat toimenpiteet
function userStory()
{
    var xhttp4 = new XMLHttpRequest();
    var req = "queries/moderaattori.php?story="+document.getElementById('userstory').value;
    xhttp4.open("GET", req, true);
    xhttp4.send();
    // User storyn luontikontrollit pois:
    hideControls();
}

// Jos mode painaa entteria user storyssa
function checkKey2(e)
{
    if(e.keyCode == 13)
    {
        userStory();
    }
}

// Selvittaa mitka moderaattorikontrollit pitaa olla paalla
function storyControls()
{
    if (storyonoff == 1) // If there is a story
    {
        hideControls();
        // cardCount(); // playercards.js
    }
    else if (storyonoff == 0) // If no story exists
    {
        showControls();
    }
}

// Pelin lopetus
function gameStop(gameid)
{
    // Poista kortit
    clearGamecards(); // playercards.js
    // Nullaa user story
    var xhttp4 = new XMLHttpRequest();
    xhttp4.open("GET", "queries/moderaattori.php?game_id="+gameid, true);
    xhttp4.send();

    showControls();
    // Tyhjenna pelaajakortit-alue
    clearCards(); // playercards.js
    // $("#consensus").remove();
}

function showControls()
{
    document.getElementById("kirjoita").style.display = "inline";
    document.getElementById("startbutton").style.display = "inline";
    document.getElementById("userstory").style.display = "inline";
    document.getElementById("stopbutton").style.display = "none";
    document.getElementById("resetbutton").style.display = "none";
}
function hideControls()
{
    document.getElementById("kirjoita").style.display = "none";
    document.getElementById("startbutton").style.display = "none";
    document.getElementById("userstory").style.display = "none";
    document.getElementById("stopbutton").style.display = "inline";
    document.getElementById("resetbutton").style.display = "inline";
}