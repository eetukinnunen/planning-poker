// Laskee pelattujen korttien ja taten korttipaikkojen maaran
function cardCount()
{
    var xhttp6 = new XMLHttpRequest();
    xhttp6.onreadystatechange = function()
    {
        if(this.readyState==4 && this.status==200)
        {
            // cardcount = pelaajakortit.php:sta saatu vastaus
            var cardcount = this.responseText;
            listCards(cardcount);
        }
    }

    xhttp6.open("GET", "queries/pelaajakortit.php?ts="+(new Date()).getTime(), true);
    xhttp6.send();
}

// Tulostaa pelaajien korttipaikat
function listCards(cardcount)
{
    clearCards(); // Tyhjentaa pois edellisen looppikierroksen kortit
    var playercount = document.getElementById('pelaajamaara').innerHTML;
    if (playercount == cardcount)
    {
        $.ajax
        ({
            type: "GET",
            url: "queries/pelattukortti.php",
            contentType: "application/json",
            dataType: 'json',
            cache: false,
            success: function (data) {
                var equality = true;
                // clearCards(); // Tyhjentaa pois edellisen looppikierroksen kortit
                for ( var i = 0; i < cardcount; i++)
                {
                    //var value = data['value'.cardcount];
                    //alert(value);
                    //var player = data['player'.cardcount];
                    var value = data.values[i];
                    // Jos arvot eivat ole samat:
                    if (value !== data.values[0])
                    {
                        equality = false;
                    }
                    var player = data.players[i];
                    $("#korttikohta").after('<li class="korttilista"><div class="korttipaikka" style="background-color: #ffffcc"><b>'+player+'<br />'+value+'</b></div></li>');
                }
                if (equality === true)
                {
                    //$("#tarina").after('<br /><h3 id="consensus">Consensus reached!</h3>');
                    swal('Planning Game Complete!', 'Consensus reached!', 'success');
                }
            }
        });
        stateSet(2); // game.js

    }
    else
    {
        for ( var i = 1; i <= cardcount; i++)
        {
            //var kortti = document.createTextNode('<li><div class="korttipaikka"></div></li>');
            //korttikentta.appendChild(kortti);
            $("#korttikohta").after('<li class="korttilista"><div class="korttipaikka"></div></li>');
        }
    }
}

// Tyhjentaa korttialueen
function clearCards()
{
    $(".korttilista").remove();

}

// Tyhjentaa pelin kortit tietokannasta
function clearGamecards()
{
    var xhttp4 = new XMLHttpRequest();
    xhttp4.open("GET", "queries/pelaajakortit.php", true);
    xhttp4.send();
}