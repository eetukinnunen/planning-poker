// Pelaajalistan printtaus
setInterval(playerList, 1000);
function playerList()
{
    var xhttp2 = new XMLHttpRequest();
    xhttp2.onreadystatechange = function()
    {
        if(this.readyState==4 && this.status==200)
        {
            var temp = this.responseText;
            temp = temp.replace(/\|/g);
            temp = temp.replace(/\n/g, "<br>");
            document.getElementById('pelaajalista').innerHTML = temp;
        }
    }
    xhttp2.open("GET", "queries/pelaajalista.php?ts="+(new Date()).getTime(), true);
    xhttp2.send();
}
// Kun pelaaja lähtee, nimi häviää pelaajat-listasta
function playerEscape(userid, gameid)
{
    var xhttp3 = new XMLHttpRequest();
    var req = "queries/pelaajalista.php?user_id="+userid;
    req = req + "&game_id="+gameid;
    xhttp3.open("GET", req, true);
    xhttp3.send();
}