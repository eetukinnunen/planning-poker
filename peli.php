<?php
include ('includes/config.php');
session_start();

if (isset($_SESSION['user']) && isset($_SESSION['gameid']))
{

    // echo $_SESSION['gameid'];
    $gameid = $_SESSION['gameid'];
    $usernick = $_SESSION['user'];

    $query = "SELECT * FROM game WHERE game_id = '".$gameid."'";
    $result = mysqli_query($conn, $query);
    while ($row = mysqli_fetch_array($result))
    {
        $gamecode = $row['gamecode'];
        $gamename = $row['game_name'];
        $moderator_id = $row['ref_moderator'];
    }
    $query = "SELECT player_id FROM player WHERE player_name = '".$usernick."'";
    $result = mysqli_query($conn, $query);
    while ($row = mysqli_fetch_array($result))
    {
        $user_id = $row['player_id'];
    }

    // You are now an active player by coming to this page
    $query = "INSERT INTO activeplayers (ref_player, ref_game) VALUES (".$user_id.", ".$gameid.")";
    $result = mysqli_query($conn, $query);

    if ($user_id == $moderator_id) // If you are the moderator
    {
        // Is the user story null or not
        $query = "SELECT user_story FROM game WHERE game_id = " . $gameid;
        $result = mysqli_query($conn, $query);
        while ($row = mysqli_fetch_array($result))
        {
            $storycheck = $row['user_story'];
        }
        if (!empty($storycheck))
        {
            $storyonoff = 1;
        }
        else
        {
            $storyonoff = 0;
        }
    }
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Planning Poker</title>
    <link rel="stylesheet" href="sweetalert/sweetalert2.min.css">
	<link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="css/new.css">

    <script src="sweetalert/sweetalert2.min.js"></script>
	<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="js/chat.js"></script>
    <script src="js/playerlist.js"></script>
    <script src="js/moderator.js"></script>
    <script src="js/game.js"></script>
    <script src="js/dargi.js"></script>
    <script src="js/playercards.js"></script>
    <script src="js/cardplayed.js"></script>
    <script>
        var userid = '<?php echo $user_id; ?>';
        var gameid = '<?php echo $gameid; ?>';
        var storyonoff = parseInt('<?php echo $storyonoff; ?>'); // -> moderator.js

        window.onload=function()
        {
            displayStory(); // game.js
            storyControls(); // moderator.js
        };
    </script>

</head>
<body onbeforeunload="return playerEscape(userid, gameid)" onunload="return playerEscape(userid, gameid)">
<div class="container">

    <div class="korttialue">
        <h2>Player cards</h2>

        <ul id="korttipaikat" >
            <li id="korttikohta"></li>
            <!--<li><div class="korttipaikka"></div></li>
            <li><div class="korttipaikka"></div></li>
            <li><div class="korttipaikka"></div></li>-->
        </ul>

    </div>
    <div class="contenthug">
        <div class="contentbox" id="header">
            <h2>User story: </h2>
            <p id="tarina"></p>
            <?php
            if ($user_id == $moderator_id)
            {
            ?>
                <p id="kirjoita">Write user story:</p>
                <input type="text" id="userstory" maxlength="100" onkeypress="checkKey2(event);">
                <button id="startbutton" onclick="userStory();">Start game</button>
                <!--<button id="savebutton" onclick="aktiiviKortti();">Save card</button>-->
                <button id="resetbutton" onclick="roundRestart();">Restart user story</button>
                <button id="stopbutton" onclick="gameStop(gameid);">Stop game</button>
                <br /><br />
                <label for="code">Game code:</label>
                <input type="text" name="code" value="<?php echo $gamecode; ?>" readonly="readonly">
            <?php
            }
            ?>
        </div>

        <div class="pelissa">
            <form method="post" action="#">
                <h2>Welcome to game "<?php echo $gamename; ?>", <?php echo $_SESSION['user']; ?>!</h2>

                <div class="pelikentta">   </div>

                <div class="korttipoyta">

                    <ul style="list-style-type:none" class="kpoyta" >
                         
                        <li> <div class="kortti" id="kortti0"> 0 </div> </li>
                        <li> <div class="kortti" id="kortti1"> 1 </div> </li>
                        <li> <div class="kortti" id="kortti2"> 2 </div> </li>
                        <li> <div class="kortti" id="kortti3"> 3 </div> </li>
                        <li> <div class="kortti" id="kortti5"> 5 </div> </li>
                        <li> <div class="kortti" id="kortti8"> 8 </div> </li>
                        <li> <div class="kortti" id="kortti13"> 13 </div> </li>
                        <li> <div class="kortti" id="kortti21"> 21 </div> </li>
                        <li> <div class="kortti" id="kortti34"> 34 </div> </li>
                        <li> <div class="kortti" id="kortti55"> 55 </div> </li>
                        <li> <div class="kortti" id="kortti89"> 89 </div> </li>
                    </ul>
                </div>

            </form>
        </div>
    </div>

    <div class="contenthug">
        <div class="contentbox" id="pelaajat">
            <h2>Players</h2>
            <div id="pelaajalista">
                <!-- js + php tuo tahan listan -->
            </div>
        </div>

        <div class="contentbox" id="chat">
            <h2>Chat</h2>

            <div id="chatbox">
                <!-- js + php tuo tahan chatin -->
            </div>
            <input type="hidden" id="nick" value="<?php echo htmlspecialchars($usernick); ?>">
            <input type="hidden" id="gameid" value="<?php echo $gameid; ?>">
            <input id="msg" placeholder="Say hi..." onkeypress="checkKey(event);">
            <button onclick="sendMessage();">Send</button>

            <br /><a href="/index.php" class="button">Go to Index</a>
            <p> </p>
            <a href="/valikko.php" class="button">Go to Menu</a>
            <p> </p>
            <?php
            if (isset($_SESSION['user']))
            {
                echo '<a href="logout.php" class="button">Log out</a>';
            }
            ?>
            <br /><br />
            <b>
                <p>Made by:</p>
                <p>Eetu Kinnunen, Julius Backman</p>
            </b>


        </div> <!-- END CHAT -->
    </div>

  </div> <!-- END CONTAINER -->
</body>
</html>
<?php
}
else if (!isset($_SESSION['user']))
{
    header('Location: index.php');
}
else if (!isset($_SESSION['gameid']))
{
    header('Location: index.php');
}
?>