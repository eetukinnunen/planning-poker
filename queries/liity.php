<?php
include ('../includes/config.php');
session_start();

if(isset($_POST['liity']) AND sizeof($_POST['liity']>0))
{
    $gamecode = $_POST['liity'];
    $stmt = $conn->prepare("SELECT game_id FROM game WHERE gamecode = ?");
    $stmt->bind_param("s", $gamecode);
    $stmt->execute();
    $stmt->bind_result($gameid);
    while ($stmt->fetch())
    {
        $_SESSION['gameid'] = $gameid;
    }
    header('Location: ../peli.php');
    $stmt->close();
}

?>