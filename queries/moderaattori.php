<?php
include ('../includes/config.php');
session_start();

$gameid = $_SESSION['gameid'];

// Luo user story
if (isset($_GET['story']))
{
    $userstory = $_GET['story'];

    $stmt = $conn->prepare("UPDATE game SET user_story = ?, gamestate = 1 WHERE game_id = ?");
    $stmt->bind_param("si", $userstory, $gameid);
    $stmt->execute();
}
// Nullaa pelin user story
else if (isset($_GET['game_id']))
{
    $query = "UPDATE game SET user_story = NULL, gamestate = 0 WHERE game_id = ".$gameid;
    $result = mysqli_query($conn, $query);
}

?>