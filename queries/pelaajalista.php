<?php
include '../includes/config.php';
session_start();

if (isset($_GET['ts']))
{
    $gameid = $_SESSION['gameid'];

    // Hakee moderaattorin nimen
    $query = "SELECT player_name FROM player WHERE player_id = (SELECT ref_moderator FROM game WHERE game_id = ".$gameid.")";
    $result = mysqli_query($conn, $query);
    while ($row = mysqli_fetch_array($result))
    {
        $mode = $row['player_name'];
    }

    // Listaa pelissä olevat aktiiviset pelaajat
    $query = "SELECT * FROM player WHERE player_id IN (SELECT ref_player FROM activeplayers WHERE ref_game = ".$gameid.")";
    $result = mysqli_query($conn, $query);
    $playercount = 0;
    $list = '';
    while ($row = mysqli_fetch_array($result))
    {
        $list.=$row['player_name'];
        // Moderaattorin nimen peraan:
        if ($row['player_name'] == $mode)
        {
            $list.="<b> (moderator)</b>";
        }
        $list.="\n";

        $playercount++;
    }
    echo $list;
    echo '<p id="pelaajamaara" style="visibility: hidden;">'.$playercount.'</p>';
}
// Poistuneen pelaajan poisto listasta:
else if (isset($_GET['user_id']) AND isset($_GET['game_id']))
{
    $userid = $_GET['user_id'];
    $gameid = $_GET['game_id'];

    $query = "DELETE FROM activeplayers WHERE ref_player = ".$userid." AND ref_game = ".$gameid."";
    $result = mysqli_query($conn, $query);
}
?>