<?php
header('Content-Type: application/json');
include ('../includes/config.php');
session_start();

$gameid = $_SESSION['gameid'];
$username = $_SESSION['user'];

// Tallenna kortti
if (isset($_GET['pkortti']))
{
    $card_value = $_GET['pkortti'];

    $stmt = $conn->prepare("INSERT INTO card (card_value, ref_game, ref_player) SELECT ?, ?, player_id FROM player WHERE player_name = ?");
    $stmt->bind_param("iis", $card_value, $gameid, $username);
    $stmt->execute();
}

// Tarkista onko pelaaja jo valinnut kortin
else if (isset($_GET['ts']))
{
    $query = "SELECT COUNT(*) AS cardcheck FROM card WHERE ref_game = ".$gameid." AND ref_player = (SELECT player_id FROM player WHERE player_name = '".$username."')";
    $result = mysqli_query($conn,$query);
    $data = mysqli_fetch_assoc($result);
    echo $data['cardcheck'];
}

// Korttien arvot
else
{
    $query = "SELECT card.card_value, player.player_name FROM card INNER JOIN player ON player.player_id = card.ref_player WHERE card.ref_game = ".$gameid."";
    $result = mysqli_query($conn, $query);

    $valuearray = array(); // Card values
    $playerarray = array(); // Player names
    $count = 0;
    while($row = mysqli_fetch_array($result))
    {
        $valuearray[] = $row['card_value'];
        $playerarray[] = $row['player_name'];
        $count++;
    }
    $data = array(); // Values AND names
    //for ($i = 0; $i < $count; $i++)
    //{
        //$cardarray['value'.$count] = $valuearray[$count];
        //$cardarray['player'.$count] = $playerarray[$count];
    //}
    $data['values'] = $valuearray;
    $data['players'] = $playerarray;
    echo json_encode($data);
}

?>