<?php
include ('includes/config.php');
session_start();

// Login data
if((isset($_POST['login']) AND sizeof($_POST['login']>0)) || isset($_SESSION['user'])) {
    if(!isset($_SESSION['user']))
    {
        $user = $_POST['login'];

        $stmt = $conn->prepare("INSERT INTO player (player_name) values (?)");
        $stmt->bind_param("s", $user);
        if($stmt->execute())
        {
            $_SESSION['user'] = $user;
        }
        else
        {
            echo ("<SCRIPT LANGUAGE='JavaScript'>
			    window.alert(\"This username already exists! Try using a different name.\")
			    window.location.href='index.php'
			    </SCRIPT>");
        }
    }
    $usernick = $_SESSION['user'];
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Planning Poker</title>
	<link rel="stylesheet" href="css/normalize.css">
        <link rel="stylesheet" href="css/new.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
</head>
<body>
<div class="container">
    <div class="pelit">

      
      <h2>Create a new game or join into a game</h2>
      <!--<img src="images/Bork.gif" height="300" width="460" />-->

   <form method="post" action="queries/luopeli.php">
        <h3>Create a new game</h3>
       <p><input type="text" name="luo" placeholder="Name of the game" required ></p>
       <p class="submit"><input type="submit" name="tee" value="Create game"></p>
   </form>


    <form method="post" action="queries/liity.php">
        <h3>Join into a game</h3>
        <p><input type="text" name="liity" placeholder="Game code" required></p>
        <p class="submit"><input type="submit" name="etsi" value="Go"></p>
   </form>
        <?php
        if (isset($_SESSION['gameid']))
        {
            echo '<a href="/peli.php" class="button">Return to Game</a>';
        }
        ?>
          <p> </p>
        <a href="/index.php" class="button">Go to Index</a>
          <p> </p>
          <?php
          if (isset($_SESSION['user']))
          {
              echo '<a href="logout.php" class="button">Log out</a>';
          }
          ?>
        <br /><br />
        <b>
            <p>Made by:</p>
            <p>Eetu Kinnunen, Julius Backman</p>
        </b>
    </div>

  </div>
<?php }
else
    {
        header('Location: index.php');
    }
?>
</body>
</html>